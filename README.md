![Logo](public/logo.png)

## How to run the project? 🤓

Installing dependencies

```bash
yarn
```

Change `.env.example` file to `.env.local`

Starting the project

```bash
yarn start
```

## How to run tests? ⚙️

Unit tests

```bash
yarn test
```

E2E tests

```bash
yarn cypress open
```

## Demo 👀

[https://fourthwall-task.vercel.app/](https://fourthwall-task.vercel.app/)

## Tech stack 💻

- React (CRA)
- TypeScript
- Tailwind
- Testing Library/Jest
- Cypress
- Vercel

## Fourthwall tasks

I've split doing this task into 3 days as unfortunately, I didn't have time to finish it in one sitting. To be honest, I didn't count the time, but overall it probably took me slightly above the 8h margin.

- [x] Search input,

- [x] Results table with sortable columns: `Name`, `Owner`, `Stars` and `Created at`,
      Unfortunately, the Github API for `/search/repositories` allows only to sort by `best-match` (default one), `stars`, `forks`, `help-wanted-issues`, `updated` and `name`(this one is not mentioned in the docs). In this case, I created a select element, which allows sorting by `best-match`, `stars`, and `name` - all of them with `asc` and `desc` order selection. If there'd be a possibility to sort by the fields mentioned in the task I'd probably implement it on table column header click ex. clicking on the `Name` header would sort by the `name` field. If the same header would be clicked then it'd change the order between `asc` and `desc`. Moreover, Github API has a limit for non-authorized users - 10 requests per minute.

- [x] Pagination,
      Done using `react-table` out of the box pagination functionality

- [x] Nice and clean interface, including loading and error states.
      The interface is pretty minimalistic. I used `Tailwind` for styling. Also, I've made a logo and called the app `reposeeker` 😄. Loading state is done and error states coming from the API are shown by `react-toastify`.

- [x] Caching the search results,
      Caching is done by `react-query` library.

- [x] Being able to see the exact same results for a given URL with search params,
      Search params are handled by the `react-router` library. In this app, search params can be treated kind of like a "global state" as they're used in several places in the app.

- [x] Separating business and visual code,
- [x] Testing your code (preferably Jest, Cypress),
      I've done unit tests using `testing-library` + `jest` in some places. For E2E tests I've used Cypress with which I tested the views (`Home` and `NotFound`). To be honest this was my first time using Cypress (previously I was doing only unit + integration tests) and I had some issues with types in test files where I used `@testing-library/cypress`. Unfortunetaly, I didn't manage fix them, because of the time limitation, so I left the ugliest thing I can do in TS - `//@ts-nocheck` 🙈 I'm sorry for that!

- [x] Type interfaces,
      This is done, but as I've mentioned above I had some issues with types in Cypress tests, and figuring out the types in `react-table` took me some time.

- [x] Useful git branch history,
      I didn't understand this task a bit and my bad that I haven't asked about it, so I'm not sure if I've done it right. I've split my work into several branches (`UI creation`, `Business logic`, `Test implementation`, etc.), which I of course merged to master, but I didn't remove them (which I normally did after the merge to keep the repo clean). Also, code changes in the commits might be a little bit, but normally I keep them small, so it's easier to review them.

- [x] Anything else you think might be useful here.
      As an addition, I've deployed the app to Vercel and created a logo + name, which was mentioned before

## Future improvements and known issues

- Fix the issue with Cypress test files
- Add dark mode 🌑
- Add possibility to go to some specific page in pagination
- Add i18n, so the text will not be hardcoded
- Improve RWD for table
- Add in UI a possibility to select how many results the user wants to see
- Handle error when the network connection is lost
