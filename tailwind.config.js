module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        primary: "#4540DC",
        disabled: "#8684DB",
        transparent: "transparent",
      },
      minWidth: {
        300: 300,
        500: 500,
      },
      maxWidth: {
        300: 300,
        600: 600,
      },
      borderWidth: {
        5: 5,
      },
    },
  },
  plugins: [],
}
