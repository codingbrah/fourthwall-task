import { faAd } from "@fortawesome/free-solid-svg-icons"
import { render, screen } from "@testing-library/react"
import { Icon } from ".."

describe("Icon component tests", () => {
  it("should render properly", () => {
    render(<Icon icon={faAd} />)

    expect(screen.getByTestId("icon")).toBeInTheDocument()
  })
})
