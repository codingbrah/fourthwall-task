import { FontAwesomeIcon, FontAwesomeIconProps } from "@fortawesome/react-fontawesome"

export const IconComponent = (props: FontAwesomeIconProps) => {
  return <FontAwesomeIcon {...props} data-testid="icon" />
}
