import { FormEventHandler } from "react"

export interface SearchComponentProps {
  handleSubmit: FormEventHandler<HTMLFormElement>
  placeholder: string
  initialValue?: string
}
