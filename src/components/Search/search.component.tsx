import { useState } from "react"
import { Button } from "../Button"
import { Input } from "../Input"
import { SearchComponentProps } from "./search.types"

export const SearchComponent = ({
  handleSubmit,
  placeholder,
  initialValue = "",
}: SearchComponentProps) => {
  const [value, setValue] = useState(initialValue as string)
  const handleValueChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value)
  }

  return (
    <form onSubmit={handleSubmit}>
      <Input
        type="text"
        placeholder={placeholder}
        name="search"
        onChange={handleValueChange}
        value={value}
      />
      <Button disabled={!!!value}>Search</Button>
    </form>
  )
}
