import { fireEvent, screen } from "@testing-library/react"
import { SearchComponent as Search } from "../search.component"
import { renderWrapper as render } from "../../../utils/testUtils"

describe("Search component tests", () => {
  const handleSubmit = jest.fn((e) => e.preventDefault())

  it("should render properly", () => {
    render(<Search handleSubmit={handleSubmit} placeholder="Test placeholder" />)

    expect(screen.getByRole("button", { name: /search/i })).toBeInTheDocument()
    expect(screen.getByRole("textbox")).toBeInTheDocument()
  })

  it("should be filled in and submitted", () => {
    render(<Search handleSubmit={handleSubmit} placeholder="Test placeholder" />)

    const input = screen.getByRole("textbox")
    const button = screen.getByRole("button")

    fireEvent.change(input as HTMLInputElement, { target: { value: "Test value" } })
    fireEvent.submit(button)

    expect(handleSubmit).toBeCalled()
  })
})
