import { fireEvent, render, screen } from "@testing-library/react"
import { Input } from ".."

describe("Input component tests", () => {
  it("should render properly", () => {
    render(<Input />)

    expect(screen.getByRole("textbox")).toBeInTheDocument()
  })

  it("should have a placeholder", () => {
    render(<Input placeholder="Test placeholder..." />)

    expect(screen.getByPlaceholderText(/test placeholder.../i)).toBeInTheDocument()
  })

  it("should accept values", () => {
    render(<Input />)

    const input = screen.getByRole("textbox")

    fireEvent.change(input, { target: { value: "Test input value" } })

    expect((input as HTMLInputElement).value).toBe("Test input value")
  })
})
