import classNames from "classnames"

import { InputComponentProps } from "./input.types"

export const InputComponent = ({ ...props }: InputComponentProps) => {
  const { className, ...rest } = props
  const classes = classNames("border-2 py-1 px-5 rounded-l-2xl md:min-w-300", className)

  return <input className={classes} {...rest} />
}
