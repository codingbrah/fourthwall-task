import { faArrowUp } from "@fortawesome/free-solid-svg-icons"
import { Icon } from "../Icon"

export const EmptyComponent = () => {
  return (
    <div className="flex flex-col items-center mt-5">
      <Icon icon={faArrowUp} className="text-primary animate-bounce" size="lg" />
      <p className="font-bold mt-2 text-center">
        To see some results provide repository name above and click{" "}
        <span className="text-primary">'Search'</span>
      </p>
    </div>
  )
}
