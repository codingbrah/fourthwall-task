import { render, screen } from "@testing-library/react"
import { Empty } from ".."

describe("Empty component tests", () => {
  it("should render properly", () => {
    render(<Empty />)

    expect(screen.getByTestId("icon")).toBeInTheDocument()

    expect(
      screen.getByText(/to see some results provide repository name above and click/i)
    ).toBeInTheDocument()
  })
})
