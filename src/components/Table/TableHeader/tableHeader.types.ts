import { ReactNode } from "react"
import { TableHeaderProps } from "react-table"

export interface TableHeaderComponentProps extends TableHeaderProps {
  children: ReactNode
}
