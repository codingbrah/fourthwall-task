import classNames from "classnames"

import { TableHeaderComponentProps } from "./tableHeader.types"

export const TableHeaderComponent = (props: TableHeaderComponentProps) => {
  const { children, className, ...rest } = props

  const classes = classNames("bg-gray-100 px-3 py-2 text-left", className)

  return (
    <th {...rest} className={classes}>
      {children}
    </th>
  )
}
