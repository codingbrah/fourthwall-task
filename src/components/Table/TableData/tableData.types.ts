import { ReactNode } from "react"
import { TableCellProps } from "react-table"

export interface TableDataComponentProps extends TableCellProps {
  children: ReactNode
}
