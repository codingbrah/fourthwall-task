import { TableDataComponentProps } from "./tableData.types"

export const TableDataComponent = (props: TableDataComponentProps) => {
  const { children, ...rest } = props
  return (
    <td {...rest} className="px-3 py-2">
      <p className="whitespace-nowrap overflow-hidden text-ellipsis">{children}</p>
    </td>
  )
}
