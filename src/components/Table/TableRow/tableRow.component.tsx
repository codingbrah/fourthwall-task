import classNames from "classnames"
import { TableRowComponentProps } from "./tableRow.types"

export const TableRowComponent = (props: TableRowComponentProps) => {
  const { children, className, ...rest } = props

  const classes = classNames("border-b-2", className)

  return (
    <tr {...rest} className={classes}>
      {children}
    </tr>
  )
}
