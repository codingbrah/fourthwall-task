import { ReactNode } from "react"
import { TableRowProps } from "react-table"

export interface TableRowComponentProps extends TableRowProps {
  children: ReactNode
}
