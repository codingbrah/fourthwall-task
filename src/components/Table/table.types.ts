import { Dispatch, ReactNode, SetStateAction } from "react"
import {
  Column,
  HeaderGroup,
  HeaderPropGetter,
  Row,
  RowPropGetter,
  SortingRule,
  TableState,
} from "react-table"

import { SortType, TableRenderStatus } from "../../types/global"

interface RenderPaginationProps {
  canPreviousPage: boolean
  canNextPage: boolean
  pageOptions: number[]
  pageCount: number
  gotoPage: (updater: number | ((pageIndex: number) => number)) => void
  nextPage: () => void
  previousPage: () => void
  pageIndex: number
}
export interface TableComponentProps<T extends object> {
  getRowProps?: (row: Row<T>) => RowPropGetter<T> | undefined
  getSortByToggleProps?: (column: HeaderGroup<T>) => HeaderPropGetter<T> | undefined
  status: TableRenderStatus
  totalCount?: number | undefined
  data: T[]
  columns: Column<T>[]
  renderPagination?: (props: RenderPaginationProps) => ReactNode
  initialState?: Partial<TableState<T>> | undefined
  customSortBy?: Dispatch<SetStateAction<SortingRule<SortType>[]>>
  tableClasses?: string
  tableHeadClasses?: string
  tableBodyClasses?: string
  tableHeaderClasses?: string
  tableRowHeadClasses?: string
  tableRowBodyClasses?: string
  tableDataClasses?: string
}
