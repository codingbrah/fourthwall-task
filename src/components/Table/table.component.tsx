import { useEffect } from "react"

import { faChevronDown, faChevronUp } from "@fortawesome/free-solid-svg-icons"
import { HeaderGroup, usePagination, useSortBy, useTable } from "react-table"
import { Icon } from "../Icon"

import { TableComponentProps } from "./table.types"
import { TableBody } from "./TableBody"
import { TableData } from "./TableData"
import { TableHead } from "./TableHead"
import { TableHeader } from "./TableHeader"
import { TableRow } from "./TableRow"
import classNames from "classnames"

export const TableComponent = <T extends object>({
  columns,
  data,
  getRowProps = () => ({}),
  totalCount = undefined,
  renderPagination,
  initialState,
  customSortBy,
  tableClasses,
  tableHeadClasses,
  tableBodyClasses,
  tableHeaderClasses,
  tableRowHeadClasses,
  tableRowBodyClasses,
  tableDataClasses,
}: TableComponentProps<T>) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    state: { pageIndex, sortBy },
  } = useTable(
    {
      data,
      columns,
      initialState,
      manualPagination: true,
      manualSortBy: true,
      defaultCanSort: false,
      pageCount: totalCount,
    },
    useSortBy,
    usePagination
  )

  useEffect(() => {
    if (customSortBy) customSortBy(sortBy)
  }, [sortBy, customSortBy])

  const generateSortingIndicator = (column: HeaderGroup<T>) => {
    return column.isSorted ? (
      column.isSortedDesc ? (
        <Icon icon={faChevronDown} className="ml-1" />
      ) : (
        <Icon icon={faChevronUp} className="ml-1" />
      )
    ) : (
      ""
    )
  }

  const tableClassName = classNames("border-2 table-fixed w-full overflow-scroll", tableClasses)

  return (
    <>
      <table {...getTableProps()} className={tableClassName}>
        <TableHead className={tableHeadClasses}>
          {headerGroups.map((headerGroup) => (
            <TableRow {...headerGroup.getHeaderGroupProps()} className={tableRowHeadClasses}>
              {headerGroup.headers.map((column) => (
                <TableHeader
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                  className={tableHeaderClasses}
                >
                  {column.render("Header")}
                  {generateSortingIndicator(column)}
                </TableHeader>
              ))}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()} className={tableBodyClasses}>
          {page.map((row) => {
            prepareRow(row)
            return (
              <TableRow {...row.getRowProps(getRowProps(row))} className={tableRowBodyClasses}>
                {row.cells.map((cell) => {
                  return (
                    <TableData {...cell.getCellProps()} className={tableDataClasses}>
                      {cell.render("Cell")}
                    </TableData>
                  )
                })}
              </TableRow>
            )
          })}
        </TableBody>
      </table>

      {renderPagination &&
        renderPagination({
          canPreviousPage,
          canNextPage,
          pageOptions,
          pageCount,
          gotoPage,
          nextPage,
          previousPage,
          pageIndex,
        })}
    </>
  )
}
