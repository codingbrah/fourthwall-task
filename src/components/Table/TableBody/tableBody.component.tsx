import classNames from "classnames"
import { TableBodyComponentProps } from "./tableBody.types"

export const TableBodyComponent = (props: TableBodyComponentProps) => {
  const { children, className, ...rest } = props

  const classes = classNames("rounded-b-lg", className)

  return (
    <tbody {...rest} className={classes}>
      {children}
    </tbody>
  )
}
