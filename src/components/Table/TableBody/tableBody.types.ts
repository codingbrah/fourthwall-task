import { ReactNode } from "react"
import { TableBodyProps } from "react-table"

export interface TableBodyComponentProps extends TableBodyProps {
  children: ReactNode
}
