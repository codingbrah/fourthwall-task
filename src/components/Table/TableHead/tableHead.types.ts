import { ReactNode } from "react"

export interface TableHeadComponentProps {
  children: ReactNode
  className?: string
}
