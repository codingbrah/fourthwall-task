import { TableHeadComponentProps } from "./tableHead.types"

export const TableHeadComponent = (props: TableHeadComponentProps) => {
  const { children, className } = props
  return <thead className={className}>{children}</thead>
}
