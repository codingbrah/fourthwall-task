import { fireEvent, screen, waitFor } from "@testing-library/react"
import camelcaseKeys from "camelcase-keys"

import { TableRenderStatus } from "../../../types/global"

import { renderWrapper as render } from "../../../utils/testUtils"
import { mockColumns, mockGithubRepoResponse } from "../../../constants/mock"
import { PAGE_SIZE } from "../../../constants/variables"

import { Table } from ".."
import { Pagination } from "../../Pagination"
import { usePages } from "../../../pages/Home/hooks/usePages/usePages"

const mockResponseCamelCase = camelcaseKeys(mockGithubRepoResponse, { deep: true })

describe("Table component tests", () => {
  it("should render properly", () => {
    render(
      <Table
        data={mockResponseCamelCase.items}
        columns={mockColumns}
        totalCount={mockResponseCamelCase.totalCount}
        status={TableRenderStatus.SUCCESS}
      />
    )

    expect(screen.getByRole("table")).toBeInTheDocument()
  })

  it("should display column names properly", () => {
    render(
      <Table
        data={mockResponseCamelCase.items}
        columns={mockColumns}
        totalCount={mockResponseCamelCase.totalCount}
        status={TableRenderStatus.SUCCESS}
      />
    )

    const columns = screen.getAllByRole("columnheader")

    expect(columns).toHaveLength(4)

    columns.forEach((column, index) => {
      expect(column).toHaveTextContent(mockColumns[index].Header!)
    })
  })

  it("should display body data properly", () => {
    render(
      <Table
        data={mockResponseCamelCase.items}
        columns={mockColumns}
        totalCount={mockResponseCamelCase.totalCount}
        status={TableRenderStatus.SUCCESS}
      />
    )
    const cell = screen.getByRole("cell", { name: "omniauth-facebook" })
    expect(cell).toBeInTheDocument()
  })

  it("should have working pagination", async () => {
    const { totalPages } = usePages({ totalCount: mockResponseCamelCase.totalCount, page: null })

    render(
      <Table
        data={mockResponseCamelCase.items}
        columns={mockColumns}
        totalCount={totalPages}
        status={TableRenderStatus.SUCCESS}
        renderPagination={(props) => <Pagination {...props} />}
      />
    )

    const pagination = screen.getByTestId("pagination")
    const currentPage = screen.getByTestId("current-page")
    const totalCount = screen.getByTestId("total-count")
    const previousButton = screen.queryByRole("button", { name: /previous/i })
    const nextButton = screen.queryByRole("button", { name: /next/i })

    const totalCountValue = Math.ceil(mockResponseCamelCase.totalCount / PAGE_SIZE).toString()

    expect(pagination).toBeInTheDocument()
    expect(previousButton).not.toBeInTheDocument()
    expect(nextButton).toBeInTheDocument()
    expect(currentPage).toHaveTextContent("1")
    expect(totalCount).toHaveTextContent(totalCountValue)

    fireEvent.click(nextButton as HTMLButtonElement)
    expect(currentPage).toHaveTextContent("2")

    await waitFor(() => {
      expect(screen.getByRole("button", { name: /previous/i })).toBeInTheDocument()
    })

    fireEvent.click(screen.getByRole("button", { name: /previous/i }))
    expect(currentPage).toHaveTextContent("1")
  })
})
