export interface PaginationComponentProps {
  canPreviousPage: boolean
  canNextPage: boolean
  pageIndex: number
  pageOptions: number[]
  handleNextPage: () => void
  handlePreviousPage: () => void
}

export interface PaginationContainerProps {
  previousPage: () => void
  nextPage: () => void
  pageCount: number
  gotoPage: (updater: number | ((pageIndex: number) => number)) => void
  canPreviousPage: boolean
  canNextPage: boolean
  pageIndex: number
  pageOptions: number[]
  customNextPageFunc?: () => void
  customPreviousPageFunc?: () => void
  currentIndex: number
}
