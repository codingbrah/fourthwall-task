import { useEffect } from "react"
import { PaginationComponent } from "./pagination.component"
import { PaginationContainerProps } from "./pagination.types"

export const PaginationContainer = (props: PaginationContainerProps) => {
  const {
    previousPage,
    nextPage,
    customNextPageFunc,
    customPreviousPageFunc,
    currentIndex,
    gotoPage,
  } = props

  const handlePreviousPage = () => {
    customPreviousPageFunc && customPreviousPageFunc()
    previousPage()
  }

  const handleNextPage = () => {
    customNextPageFunc && customNextPageFunc()
    nextPage()
  }

  useEffect(() => {
    if (currentIndex === 0) gotoPage(0)
  }, [currentIndex, gotoPage])

  return (
    <PaginationComponent
      {...props}
      handlePreviousPage={handlePreviousPage}
      handleNextPage={handleNextPage}
    />
  )
}
