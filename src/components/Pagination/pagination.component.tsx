import { faChevronLeft, faChevronRight } from "@fortawesome/free-solid-svg-icons"

import { Icon } from "../Icon"

import { PaginationComponentProps } from "./pagination.types"

export const PaginationComponent = ({
  canPreviousPage,
  canNextPage,
  pageIndex,
  pageOptions,
  handleNextPage,
  handlePreviousPage,
}: PaginationComponentProps) => {
  return (
    <div className="mt-5" data-testid="pagination">
      {canPreviousPage && (
        <button
          onClick={handlePreviousPage}
          disabled={!canPreviousPage}
          className="cursor-pointer mr-2"
          name="previous"
        >
          <Icon icon={faChevronLeft} />
          <span className="sr-only">Previous</span>
        </button>
      )}
      <span className="font-bold">
        <span data-testid={"current-page"}>{pageIndex + 1}</span> of{" "}
        <span data-testid={"total-count"}>{pageOptions.length}</span>
      </span>
      {canNextPage && (
        <button
          onClick={handleNextPage}
          disabled={!canNextPage}
          className="cursor-pointer ml-2"
          name="next"
        >
          <Icon icon={faChevronRight} />
          <span className="sr-only">Next</span>
        </button>
      )}
    </div>
  )
}
