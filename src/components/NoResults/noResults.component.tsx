import { NoResultsComponentProps } from "./noResults.types"

export const NoResultsComponent = ({ message }: NoResultsComponentProps) => {
  return <p className="mt-5 font-bold">{message}</p>
}
