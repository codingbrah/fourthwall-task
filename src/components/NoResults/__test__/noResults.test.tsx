import { render, screen } from "@testing-library/react"
import { NoResults } from ".."

describe("NoResults component tests", () => {
  it("should render properly", () => {
    render(<NoResults message="Test no results message" />)

    expect(screen.getByText(/test no results message/i)).toBeInTheDocument()
  })
})
