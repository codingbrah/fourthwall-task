import classNames from "classnames"

import { ButtonComponentProps } from "./button.types"

export const ButtonComponent = (props: ButtonComponentProps) => {
  const { children, className, disabled, ...rest } = props
  const classes = classNames(
    "border-2 border-primary px-4 py-1 text-white rounded-r-2xl",
    { "bg-disabled border-disabled": disabled },
    { "bg-primary hover:bg-white hover:text-primary": !disabled },
    className
  )

  return (
    <button className={classes} disabled={disabled} {...rest}>
      {children}
    </button>
  )
}
