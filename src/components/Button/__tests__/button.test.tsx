import { render, fireEvent, screen } from "@testing-library/react"

import { Button } from ".."

describe("Button component tests", () => {
  const handleClick = jest.fn()
  it("should render properly", () => {
    render(<Button>Test</Button>)

    expect(screen.getByRole("button", { name: /test/i })).toBeInTheDocument()
  })

  it("should be clickable", () => {
    render(<Button onClick={handleClick}>Test</Button>)

    fireEvent.click(screen.getByRole("button", { name: /test/i }))

    expect(handleClick).toHaveBeenCalledTimes(1)
  })

  it("should be disabled", () => {
    render(
      <Button onClick={handleClick} disabled={true}>
        Test
      </Button>
    )

    fireEvent.click(screen.getByRole("button", { name: /test/i }))

    expect(screen.getByRole("button", { name: /test/i })).toBeDisabled()

    expect(handleClick).toHaveBeenCalledTimes(0)
  })
})
