import { render, screen } from "@testing-library/react"
import { Loader } from ".."

describe("Loader component tests", () => {
  it("should render properly", () => {
    render(<Loader />)

    expect(screen.getByText(/loading/i)).toBeInTheDocument()
  })
})
