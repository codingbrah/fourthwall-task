export const LoaderComponent = () => {
  return (
    <div className="mt-10">
      <div className="border-t-transparent border-solid animate-spin rounded-full border-primary border-5 h-16 w-16">
        <p className="sr-only">Loading...</p>
      </div>
    </div>
  )
}
