export interface GetRepositoriesParams {
  q?: string
  sort?: string
  order?: string
  page?: number
}

type Owner = {
  login: string
  id: number
  avatarUrl: string
  gravatarId: string
  url: string
  receivedEventsUrl: string
  type: string
  htmlUrl: string
  followersUrl: string
  followingUrl: string
  gistsUrl: string
  starredUrl: string
  subscriptionsUrl: string
  organizationsUrl: string
  reposUrl: string
  eventsUrl: string
  siteAdmin: boolean
}

export interface GetRepositoriesResponseItem {
  id: number
  nodeId: string
  name: string
  fullName: string
  owner: Owner
  htmlUrl: string
  description: string
  url: string
  createdAt: string
  stargazersCount: string
}

export interface GetRepositoriesResponse {
  total_count: number
  incomplete_results: boolean
  items: GetRepositoriesResponseItem[]
}

export enum TableRenderStatus {
  IDLE = "IDLE",
  NO_RESULTS = "NO_RESULTS",
  ERROR = "ERROR",
  PENDING = "PENDING",
  SUCCESS = "SUCCESS",
}

export type DateType = {
  createdAt: string
}

export type SortType = {
  id: string
  desc: boolean
}
