import axios from "axios"

export const client = axios.create({
  headers: {
    Authorization: `token ${process.env.REACT_APP_GITHUB_AUTH_TOKEN}`,
  },
})
