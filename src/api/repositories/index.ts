import { client } from "../client"
import { apiUrl } from "../helpers"
import camelcaseKeys from "camelcase-keys"
import { PAGE_SIZE } from "../../constants/variables"
import { GetRepositoriesParams } from "../../types/global"
import { AxiosError } from "axios"

const REPOSITORIES_URL = apiUrl("/search/repositories")

export const getRepositories = async (params: GetRepositoriesParams) => {
  try {
    const res = await client.get(REPOSITORIES_URL, {
      params: {
        per_page: PAGE_SIZE,
        ...params,
      },
    })
    return camelcaseKeys(res.data, { deep: true })
  } catch (error) {
    throw Error((error as AxiosError).response?.data.message)
  }
}
