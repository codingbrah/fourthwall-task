import { apiConfig } from "../config"

export const apiUrl = (value: string) => apiConfig + value
