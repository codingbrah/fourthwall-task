import { renderHook } from "@testing-library/react-hooks"
import { MemoryRouter } from "react-router-dom"
import { waitFor } from "@testing-library/react"
import TestRenderer from "react-test-renderer"
import { ReactNode } from "react"

import { useQueryParams } from "../useQueryParams"

interface WrapperProps {
  children: ReactNode
}

const { act } = TestRenderer

const initialEntries = [`/?q=test`]

const wrapper = ({ children }: WrapperProps) => (
  <MemoryRouter initialEntries={initialEntries}>{children}</MemoryRouter>
)

const render = () => renderHook(() => useQueryParams(), { wrapper })

describe("useQueryParams hook tests", () => {
  it("should return initial query param", async () => {
    const { result } = render()
    await waitFor(() =>
      expect(JSON.stringify(result.current.queryParams)).toStrictEqual(
        JSON.stringify({ q: "test" })
      )
    )
  })

  it("should return new query param", async () => {
    const { result } = render()
    act(() => result.current.setQueryParams({ q: "fourthwall" }))
    await waitFor(() =>
      expect(JSON.stringify(result.current.queryParams)).toStrictEqual(
        JSON.stringify({ q: "fourthwall" })
      )
    )
  })

  it("should return updated query param", async () => {
    const { result } = render()
    act(() => result.current.addQueryParams({ sort: "name", order: "asc" }))
    await waitFor(() =>
      expect(JSON.stringify(result.current.queryParams)).toStrictEqual(
        JSON.stringify({ order: "asc", q: "test", sort: "name" })
      )
    )
  })

  it("should remove one of the query param", async () => {
    const { result } = render()
    act(() => result.current.addQueryParams({ sort: "name", order: "asc" }))
    act(() => result.current.removeQueryParam(["order"]))
    await waitFor(() =>
      expect(JSON.stringify(result.current.queryParams)).toStrictEqual(
        JSON.stringify({ q: "test", sort: "name" })
      )
    )
  })
})
