import { useLocation, useSearchParams } from "react-router-dom"
import { omit } from "ramda"
import queryString from "query-string"

export const useQueryParams = () => {
  const location = useLocation()
  const [, setSearchParams] = useSearchParams()

  const searchQueryParams = location.search
  const queryParams = queryString.parse(searchQueryParams)

  const setQueryParams = (params: object) => {
    const stringified = queryString.stringify(params)
    setSearchParams(stringified)
  }

  const addQueryParams = (params: object) => {
    const newQueryParams = queryString.stringify({ ...queryParams, ...params })
    setSearchParams(newQueryParams)
  }

  const removeQueryParam = (name: string[]) => {
    const cleanedQueryParams = omit(name, queryParams)
    const newQueryParams = queryString.stringify(cleanedQueryParams)
    setSearchParams(newQueryParams)
  }

  return { queryParams, setQueryParams, addQueryParams, removeQueryParam }
}
