import { isEmpty } from "ramda"
import { useState, useEffect } from "react"
import { SortingRule } from "react-table"
import { SortType, TableRenderStatus } from "../../types/global"
import { useQueryParams } from "../useQueryParams/useQueryParams"
import { UseCustomSortProps } from "./useCustomSort.types"

export const useCustomSort = ({ tableStatus }: UseCustomSortProps) => {
  const { queryParams, addQueryParams, removeQueryParam } = useQueryParams()
  const [sortBy, setSortBy] = useState<SortingRule<SortType>[]>([])

  const clearSortQuery = { order: undefined, sort: undefined }
  const sortByItem = sortBy[0]
  const order = sortByItem?.desc ? "desc" : "asc"
  const sort = sortByItem?.id
  const sortQuery = sort ? { order, sort } : clearSortQuery

  const id = queryParams.sort
  const desc = queryParams.order === "desc" ? true : false
  const initialSortByValue = [{ id, desc }]

  useEffect(() => {
    if (!isEmpty(sortBy)) {
      addQueryParams(sortQuery)
    }

    if (isEmpty(sortBy) && tableStatus === TableRenderStatus.SUCCESS) {
      removeQueryParam(["order", "sort"])
    }

    // sortQuery is changed every render, so it can't be in dependency array
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sortBy, tableStatus])

  return { initialSortByValue, setSortBy }
}
