import { renderHook } from "@testing-library/react-hooks"
import { TableRenderStatus } from "../../../types/global"

import { useCustomSort } from "../useCustomSort"

jest.mock("../../useQueryParams/useQueryParams", () => ({
  useQueryParams: () => ({
    queryParams: { q: "test", sort: "name", order: "asc" },
    setQueryParams: jest.fn(),
    addQueryParams: jest.fn(),
    removeQueryParam: jest.fn(),
  }),
}))

const render = () => renderHook(() => useCustomSort({ tableStatus: TableRenderStatus.SUCCESS }))

describe("useCustomSort hook tests", () => {
  it("should return initialSortByValue", async () => {
    const { result } = render()

    expect(JSON.stringify(result.current.initialSortByValue)).toStrictEqual(
      JSON.stringify([{ id: "name", desc: false }])
    )
  })
})
