import { TableRenderStatus } from "../../types/global"

export interface UseCustomSortProps {
  tableStatus: TableRenderStatus
}
