import { toast, ToastOptions } from "react-toastify"

import { UseErrorProps } from "./useError.types"

const defaultOptions: Partial<ToastOptions> = {
  position: "top-right",
  autoClose: 5000,
  hideProgressBar: false,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
}

export const useError = (options: UseErrorProps = {}) => {
  const handleError = (message: string) =>
    toast.error(message, {
      ...defaultOptions,
      ...options,
    })
  return { handleError }
}
