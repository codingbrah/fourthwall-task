import { ToastContainer } from "react-toastify"
import { render, waitFor, act, screen } from "@testing-library/react"

import { useError } from "../useError"
const options = {
  autoClose: 1000,
}

const renderToastProviderWithHook = () => {
  const hook: { current: ReturnType<typeof useError> } = {
    current: {} as ReturnType<typeof useError>,
  }
  const Component: React.FC = () => {
    hook.current = useError(options)
    return null
  }
  const view = render(
    <div>
      <ToastContainer />
      <Component />
    </div>
  )
  return {
    ...view,
    hook,
  }
}

describe("useError hook tests", () => {
  it("should render the error message", async () => {
    const { hook } = renderToastProviderWithHook()

    act(() => {
      hook.current.handleError("error message")
    })
    await waitFor(() => {
      expect(screen.getByText("error message")).toBeInTheDocument()
    })
  })

  it("should dissapear after 1s", async () => {
    const { hook } = renderToastProviderWithHook()

    act(() => {
      hook.current.handleError("error message")
    })
    await waitFor(() => {
      expect(screen.queryByText("error message")).not.toBeInTheDocument()
    })
  })
})
