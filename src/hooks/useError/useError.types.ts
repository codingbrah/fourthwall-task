import { ToastOptions } from "react-toastify"

export interface UseErrorProps extends ToastOptions<{}> {}
