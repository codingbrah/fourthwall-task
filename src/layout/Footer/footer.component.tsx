import CodingbrahLogo from "../../assets/images/codingbrah-logo.svg"

export const FooterComponent = () => {
  return (
    <footer className="flex justify-center items-center h-12">
      <a href="https://bio.codingbrah.com" target="_blank" rel="noreferrer">
        <img src={CodingbrahLogo} alt="Codingbrah logo" className="w-20" />
      </a>
    </footer>
  )
}
