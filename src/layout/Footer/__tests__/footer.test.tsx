import { render, screen } from "@testing-library/react"

import { Footer } from ".."

describe("Footer component tests", () => {
  it("should render properly", () => {
    render(<Footer />)

    expect(screen.getByRole("link", { name: /codingbrah logo/i })).toBeInTheDocument()
  })
})
