import { screen } from "@testing-library/react"

import { renderWrapper as render } from "../../../utils/testUtils"

import { Header } from ".."

describe("Header component tests", () => {
  it("should render properly", () => {
    render(<Header />)

    expect(screen.getByRole("img", { name: /reposeeker logo/i })).toBeInTheDocument()
  })
})
