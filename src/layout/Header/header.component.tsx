import RepoSeekerLogo from "../../assets/images/reposeeker-logo.svg"

export const HeaderComponent = () => {
  return (
    <header className="flex justify-center items-center">
      <img src={RepoSeekerLogo} alt="RepoSeeker logo" className="w-64 mt-4" />
    </header>
  )
}
