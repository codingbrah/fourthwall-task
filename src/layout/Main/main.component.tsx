import { MainProps } from "./main.types"

export const MainComponent = ({ children }: MainProps) => {
  return <main className="flex flex-col flex-1 items-center w-full h-full py-5">{children}</main>
}
