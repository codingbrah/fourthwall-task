import { Footer } from "../Footer"
import { Header } from "../Header"
import { Main } from "../Main"

import { BasicLayoutProps } from "./basicLayout.types"

export const BasicLayoutComponent = ({ children }: BasicLayoutProps) => {
  return (
    <div className="flex justify-center min-h-screen">
      <div className="flex flex-col justify-center max-w-600 px-3">
        <Header />
        <Main>{children}</Main>
        <Footer />
      </div>
    </div>
  )
}
