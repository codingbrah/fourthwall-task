import { ReactNode } from "react"

export interface BasicLayoutProps {
  children: ReactNode
}
