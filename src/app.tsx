import { Routes, Route, Navigate } from "react-router-dom"
import { ToastContainer } from "react-toastify"

import { Home } from "./pages/Home"
import { NotFound } from "./pages/NotFound"

import "react-toastify/dist/ReactToastify.css"
import { ROUTES } from "./constants/routes"

export const App = () => (
  <>
    <Routes>
      <Route path={ROUTES.home} element={<Home />} />
      <Route path={ROUTES.notFound} element={<NotFound />} />
      <Route path="*" element={<Navigate to={ROUTES.notFound} />} />
    </Routes>
    <ToastContainer />
  </>
)
