import { NavLink } from "react-router-dom"

import { BasicLayout } from "../../layout/BasicLayout"

export const NotFoundComponent = () => {
  return (
    <BasicLayout>
      <div className="flex flex-col flex-1 justify-center items-center">
        <h2 className="font-bold text-3xl text-center">Page Not Found! 😢</h2>
        <NavLink to="/" className="font-bold hover:text-primary mt-4">
          Go to the homepage
        </NavLink>
      </div>
    </BasicLayout>
  )
}
