import { NotFoundComponent } from "./notFound.component"
import { Helmet } from "react-helmet-async"

export const NotFoundContainer = () => {
  return (
    <>
      <Helmet>
        <title>Not found - reposeeker</title>
        <meta name="description" content="The page you were trying to enter doesn't exist" />
      </Helmet>
      <NotFoundComponent />
    </>
  )
}
