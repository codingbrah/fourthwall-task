import { Dispatch, FormEventHandler, SetStateAction } from "react"
import { Column, SortingRule } from "react-table"

import { GetRepositoriesResponseItem, SortType, TableRenderStatus } from "../../types/global"

type TableInitialState = {
  pageIndex: number
  pageSize: number
  hiddenColumns: string[]
}
export interface HomeComponentProps {
  data: GetRepositoriesResponseItem[]
  columns: Column<GetRepositoriesResponseItem>[]
  totalCount: number | undefined
  status: TableRenderStatus
  tableInitialState: TableInitialState
  searchInitialValue: string
  handleSearchSubmit: FormEventHandler<HTMLFormElement>
  customNextPageFunc: () => void
  customPreviousPageFunc: () => void
  customSortBy: Dispatch<SetStateAction<SortingRule<SortType>[]>>
  currentIndex: number
}
