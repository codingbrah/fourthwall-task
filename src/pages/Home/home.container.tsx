import { FormEvent, useMemo } from "react"
import { useQuery } from "react-query"
import { Helmet } from "react-helmet-async"
import { isEmpty } from "ramda"

import { getRepositories } from "../../api/repositories"
import { PAGE_SIZE } from "../../constants/variables"

import { repositoryTableColumns } from "./home.constants"
import { HomeComponent } from "./home.component"

import { useStatus } from "./hooks/useStatus"
import { usePages } from "./hooks/usePages"
import { useError } from "../../hooks/useError"
import { useQueryParams } from "../../hooks/useQueryParams"
import { useCustomSort } from "../../hooks/useCustomSort"

export const HomeContainer = () => {
  const { queryParams, setQueryParams, addQueryParams } = useQueryParams()

  const { handleError } = useError()

  const { data, isLoading, isSuccess, isError } = useQuery(
    ["repositories", queryParams],
    () => getRepositories(queryParams),
    {
      enabled: !isEmpty(queryParams),
      onError: (error: Error) => handleError(error.message),
      keepPreviousData: true,
    }
  )

  const memoizedData = useMemo(() => data?.items, [data])
  const columns = useMemo(() => repositoryTableColumns, [])

  const { status } = useStatus({
    isDataAvailable: !!data?.items?.length,
    isLoading,
    isSuccess,
    isError,
  })

  const { setSortBy, initialSortByValue } = useCustomSort({ tableStatus: status })

  const { totalPages, pageNumber, pageIndex } = usePages({
    totalCount: data?.totalCount,
    page: queryParams?.page,
  })

  const tableInitialState = {
    pageIndex: pageIndex,
    pageSize: PAGE_SIZE,
    hiddenColumns: ["url"],
    sortBy: initialSortByValue,
  }

  const searchInitialValue = queryParams?.q ? (queryParams.q as string) : ""

  const handleSearchSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    const repoName = (event.target as HTMLFormElement).search.value
    const repoNameParam = { q: repoName, page: 1 }
    setQueryParams(repoNameParam)
  }

  const customNextPageFunc = () => {
    if (pageNumber) {
      addQueryParams({ page: pageNumber + 1 })
    } else {
      addQueryParams({ page: 2 })
    }
  }

  const customPreviousPageFunc = () => {
    if (pageNumber) {
      addQueryParams({ page: pageNumber - 1 })
    }
  }

  return (
    <>
      <Helmet>
        <title>Home - reposeeker</title>
        <meta name="description" content="Reposeeker is the best repository search you ever seen" />
      </Helmet>
      <HomeComponent
        data={memoizedData}
        columns={columns}
        totalCount={totalPages}
        status={status}
        handleSearchSubmit={handleSearchSubmit}
        tableInitialState={tableInitialState}
        customNextPageFunc={customNextPageFunc}
        customPreviousPageFunc={customPreviousPageFunc}
        searchInitialValue={searchInitialValue}
        customSortBy={setSortBy}
        currentIndex={pageIndex}
      />
    </>
  )
}
