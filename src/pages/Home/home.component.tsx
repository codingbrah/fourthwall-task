import { Row } from "react-table"

import { BasicLayout } from "../../layout/BasicLayout"

import { Empty } from "../../components/Empty"
import { Loader } from "../../components/Loader"
import { NoResults } from "../../components/NoResults"
import { Search } from "../../components/Search"

import { Table } from "../../components/Table"

import { GetRepositoriesResponseItem, TableRenderStatus } from "../../types/global"
import { HomeComponentProps } from "./home.types"

import { Pagination } from "../../components/Pagination"

export const HomeComponent = ({
  data,
  columns,
  totalCount,
  status,
  tableInitialState,
  searchInitialValue,
  handleSearchSubmit,
  customNextPageFunc,
  customPreviousPageFunc,
  customSortBy,
  currentIndex,
}: HomeComponentProps) => {
  const renderContainerView = () => {
    switch (status) {
      case TableRenderStatus.IDLE:
        return <Empty />
      case TableRenderStatus.PENDING:
        return <Loader />
      case TableRenderStatus.NO_RESULTS:
        return <NoResults message="No results found 🙁" />
      case TableRenderStatus.SUCCESS:
        return (
          <>
            <Table
              data={data}
              columns={columns}
              getRowProps={(row: Row<GetRepositoriesResponseItem>) => ({
                onClick: () => window.open(row.values.url, "_blank")!.focus(),
                className: "",
              })}
              status={status}
              totalCount={totalCount}
              renderPagination={(props) => (
                <Pagination
                  {...props}
                  customNextPageFunc={customNextPageFunc}
                  customPreviousPageFunc={customPreviousPageFunc}
                  currentIndex={currentIndex}
                />
              )}
              initialState={tableInitialState}
              customSortBy={customSortBy}
              tableClasses="mt-8"
              tableRowBodyClasses="cursor-pointer hover:bg-blue-100"
            />
          </>
        )
      default:
        return <Empty />
    }
  }

  return (
    <BasicLayout>
      <h2 className="font-bold text-2xl mb-7 text-center">
        <span className="text-primary ">Best</span> repository search you ever seen 🤯
      </h2>
      <Search
        handleSubmit={handleSearchSubmit}
        placeholder="Repository name..."
        initialValue={searchInitialValue}
      />
      {renderContainerView()}
    </BasicLayout>
  )
}
