import { format } from "date-fns"
import { Column } from "react-table"

import { DateType, GetRepositoriesResponseItem } from "../../types/global"

export const repositoryTableColumns: Column<GetRepositoriesResponseItem>[] = [
  {
    Header: "Name",
    accessor: "name",
  },
  {
    Header: "Owner",
    //@ts-ignore
    accessor: "owner.login",
    disableSortBy: true,
  },
  {
    Header: "Stars",
    accessor: "stargazersCount",
    id: "stars",
  },
  {
    Header: "Created at",
    accessor: (date: DateType) => format(new Date(date.createdAt), "dd.MM.yyyy"),
    disableSortBy: true,
  },
  { id: "url", accessor: (data: GetRepositoriesResponseItem) => data.htmlUrl },
]
