import { PAGE_SIZE } from "../../../../constants/variables"
import { UsePagesProps } from "./usePages.types"

export const usePages = ({ totalCount, page }: UsePagesProps) => {
  const parsedPage = page && parseInt(page as string)

  const totalPages = totalCount && Math.ceil(totalCount / PAGE_SIZE)
  const pageIndex = parsedPage && parsedPage > 0 ? parsedPage - 1 : 0
  const pageNumber = parsedPage

  return { totalPages, pageNumber, pageIndex }
}
