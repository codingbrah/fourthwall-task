export interface UsePagesProps {
  totalCount: number | undefined
  page: string | string[] | null
}
