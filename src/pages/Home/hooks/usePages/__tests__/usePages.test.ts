import { renderHook } from "@testing-library/react-hooks"

import { PAGE_SIZE } from "../../../../../constants/variables"
import { usePages } from "../usePages"

const defaultArgs = { totalCount: 1000, page: "2" }

const render = (args = defaultArgs) => renderHook(() => usePages({ ...args }))

describe("usePages hook tests", () => {
  it("should return total pages number", () => {
    const totalPages = Math.ceil(defaultArgs.totalCount / PAGE_SIZE)
    const { result } = render()
    expect(result.current.totalPages).toBe(totalPages)
  })

  it("should return page number", () => {
    const { result } = render()
    expect(result.current.pageNumber).toBe(2)
  })

  it("should return page index", () => {
    const { result } = render()
    expect(result.current.pageIndex).toBe(1)
  })
})
