import { TableRenderStatus } from "../../../../types/global"
import { UseStatusProps } from "./useStatus.types"

export const useStatus = ({ isDataAvailable, isLoading, isSuccess, isError }: UseStatusProps) => {
  let status: TableRenderStatus = TableRenderStatus.IDLE

  if (!isDataAvailable && isLoading) {
    status = TableRenderStatus.PENDING
  }

  if (isDataAvailable && !isLoading && isSuccess) {
    status = TableRenderStatus.SUCCESS
  }

  if (!isDataAvailable && !isLoading && (isSuccess || isError)) {
    status = TableRenderStatus.NO_RESULTS
  }
  return { status }
}
