export interface UseStatusProps {
  isDataAvailable: boolean
  isLoading: boolean
  isSuccess: boolean
  isError: boolean
}
