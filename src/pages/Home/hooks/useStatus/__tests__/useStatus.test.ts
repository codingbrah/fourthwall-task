import { renderHook } from "@testing-library/react-hooks"

import { TableRenderStatus } from "../../../../../types/global"
import { useStatus } from "../useStatus"

const defaultArgs = { isDataAvailable: false, isLoading: false, isSuccess: false, isError: false }

const render = (args = defaultArgs) => renderHook(() => useStatus({ ...args }))

describe("useStatus hook tests", () => {
  it("should return status IDLE", () => {
    const { result } = render()
    expect(result.current.status).toBe(TableRenderStatus.IDLE)
  })

  it("should return status NO_RESULTS", () => {
    const noResultsArgs = {
      isDataAvailable: false,
      isLoading: false,
      isSuccess: true,
      isError: false,
    }
    const { result } = render(noResultsArgs)
    expect(result.current.status).toBe(TableRenderStatus.NO_RESULTS)
  })

  it("should return status SUCCESS", () => {
    const successArgs = {
      isDataAvailable: true,
      isLoading: false,
      isSuccess: true,
      isError: false,
    }
    const { result } = render(successArgs)
    expect(result.current.status).toBe(TableRenderStatus.SUCCESS)
  })

  it("should return status PENDING", () => {
    const successArgs = {
      isDataAvailable: false,
      isLoading: true,
      isSuccess: false,
      isError: false,
    }
    const { result } = render(successArgs)
    expect(result.current.status).toBe(TableRenderStatus.PENDING)
  })
})
