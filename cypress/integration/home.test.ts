//@ts-nocheck
/* eslint-disable */

import { some } from "cypress/types/lodash"

describe("Home view tests", () => {
  it("should access the page", () => {
    cy.visit("/")
  })

  it("should display empty message", () => {
    cy.findByText("To see some results provide repository name above and click").should("exist")
  })

  it("should not able to search without filling search input", () => {
    cy.findByRole("button", { name: /search/i }).should("be.disabled")
  })

  it("should be able to perform search", () => {
    cy.findByRole("textbox").type("facebook")
    cy.findByRole("button", { name: /search/i })
      .should("not.be.disabled")
      .click()
  })

  it("should see the table with data", () => {
    cy.findByRole("table").should("exist")
    cy.findByRole("table").contains("td", "facebook")
  })

  it("should sort data", () => {
    const oldCells = cy.findAllByRole("cell")

    cy.get("th").contains(/name/i).should("exist").click()

    const newCells = cy.findAllByRole("cell")

    expect(newCells).to.not.deep.eq(oldCells)
  })

  it("should change the search value", () => {
    cy.findByRole("textbox")
      .invoke("val")
      .then((oldValue) => {
        cy.findByRole("textbox").clear().type("fourthwall")

        cy.findByRole("textbox")
          .invoke("val")
          .should((newValue) => {
            expect(oldValue).not.to.eq(newValue)
          })
      })

    cy.findByRole("button", { name: /search/i })
      .should("not.be.disabled")
      .click()

    cy.findByRole("table").should("exist")
    cy.findByRole("table").contains("td", "fourthwall")
  })

  it("should display the same value in search input after reload", () => {
    cy.findByRole("textbox")
      .invoke("val")
      .then((oldValue) => {
        cy.reload()

        cy.findByRole("textbox")
          .invoke("val")
          .should((newValue) => {
            expect(oldValue).to.eq(newValue)
          })
      })
  })
})
