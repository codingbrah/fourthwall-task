//@ts-nocheck
/* eslint-disable */

describe("Empty view tests", () => {
  it("should visits page that doesn't exist", () => {
    cy.visit("/strange-path")
  })

  it("should see page not found message", () => {
    cy.findByText(/page not found/i).should("exist")
  })

  it("should go back to homepage", () => {
    cy.findByRole("link", { name: /go to the homepage/i })
      .should("exist")
      .click()
    cy.location("pathname").should("eq", "/")
  })
})
